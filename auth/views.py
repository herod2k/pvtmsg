# Create your views here.
import sys,json,base64,datetime
from django.contrib.auth import authenticate, login, logout
from django.shortcuts import render_to_response,HttpResponse,HttpResponseRedirect
from django.template import RequestContext
from django.views.decorators.csrf import csrf_protect
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from models import *
from userprofile.models import UserProfile,Publickey,Transmissionkey,Friendship
from django.utils.translation import ugettext as _
from django.core.mail import send_mail
from django.http import Http404, HttpResponseForbidden
from django.conf import settings


def check_browser(request):
	result=False
	useragent=request.META['HTTP_USER_AGENT'].lower()
	keys={}
	for val in useragent.split(' '):
		tmp=val.split('/')
		if len(tmp)>1:
			keys[tmp[0]]=tmp[1];
	if useragent[0:5]=="opera":
		result=float(keys.get('version'))>=settings.BROWSERS.get('opera')
	if useragent[0:7]=="mozilla":
		if keys.get('firefox',False):
			result=float(keys.get('firefox'))>=settings.BROWSERS.get('firefox')
		if keys.get('chrome',False):
			result=float(keys.get('chrome')[0:2])>=settings.BROWSERS.get('chrome')
	return result

@csrf_protect
def registration1(request):
	if check_browser(request)==False:
		return render_to_response('errors/browser.html')
	else:
		if request.method == 'POST':
			form = RegistrationForm(request.POST, request.FILES)
			if form.is_valid():
				#User registration
				user = User.objects.create_user(form.cleaned_data['username'], form.cleaned_data['email'], form.cleaned_data['password'])
				user.first_name = form.cleaned_data['name']
				user.last_name = form.cleaned_data['surname']
				user.save()
				
				#Profile registration
				if form.cleaned_data['friend']!='':
					date_end_of_service = datetime.datetime.today()+datetime.timedelta(60)
				else:
					date_end_of_service = datetime.datetime.today()+datetime.timedelta(30)
					
				userprofile = UserProfile(
					searchable=form.cleaned_data['searchable'],
					user=user,
					avatar = form.cleaned_data['avatar'],
					showemail = form.cleaned_data['showemail'],
					notificationbyemail = form.cleaned_data['notificationbyemail'],
					language = form.cleaned_data['language'],
					date_end_of_service=date_end_of_service)
				userprofile.save()
				
				#Send activation email
				if len(form.cleaned_data['email']) > 0:
					data={'username':form.cleaned_data['username'],'email':form.cleaned_data['email'],'domain':request.META['HTTP_HOST']}
					send_activation_email().send(data)
				user = authenticate(username=form.cleaned_data['username'], password=form.cleaned_data['password'])
				if user is not None:
					if user.is_active:
						login(request, user)
						if form.cleaned_data['friend'] is not None:
							request.session['friend']=form.cleaned_data['friend']
				return HttpResponseRedirect('/registration2/')
		else:
			form = RegistrationForm(initial={'language':request.LANGUAGE_CODE})
		return render_to_response('registration/registration.html',{
			'form': form
		},context_instance = RequestContext(request))

@login_required(login_url='/registration/')
def registration2(request):
	if (UserProfile.objects.get(user=request.user).public_keys.filter(version=settings.KEY_VERSION).count()==0) and (UserProfile.objects.get(user=request.user).transmission_keys.filter(version=settings.KEY_VERSION).count()==0):
		return render_to_response('registration/registration2.html',{},context_instance = RequestContext(request))
	else:
		return HttpResponseRedirect('/')

@login_required()
def registration_keys(request):
	result='ko'
	if request.user.is_authenticated():
		publicKeys=request.POST.get('publicKeys')
		result=publicKeys
		if publicKeys is not None:
			from M2Crypto import RSA
			from Crypto.Cipher import AES
			
			pkrsa=publicKeys.split('?')[0]
			pkaes=publicKeys.split('?')[1]
			rsa = RSA.load_key(settings.SERVER_PRIVATE_KEY,callback = lambda x : settings.PASSWORD_RSA_KEY)
			keyaes=rsa.private_decrypt (pkrsa.decode("base64"), RSA.pkcs1_padding)
			keyaes=keyaes.decode("base64")
			
			mode = AES.MODE_CBC
			decryptor = AES.new(keyaes, mode)
			keys = decryptor.decrypt(pkaes.decode("base64"))[16:]
			keys=json.loads(keys.decode("base64"))
			if keys.get(request.user.username).get(settings.KEY_VERSION).get('domain')==settings.KEY_DOMAIN:
				if len(keys.get(request.user.username).get(settings.KEY_VERSION).get('transmissionsKeys'))==settings.TRANSMISSION_NUM_KEYS:
					if (UserProfile.objects.get(user=request.user).public_keys.filter(version=settings.KEY_VERSION).count()==0) and (UserProfile.objects.get(user=request.user).transmission_keys.filter(version=settings.KEY_VERSION).count()==0):
						pkey=Publickey.objects.create(version=settings.KEY_VERSION,key_string=keys.get(request.user.username).get(settings.KEY_VERSION).get('PublicKeyString'))
						user=UserProfile.objects.get(user=request.user)
						user.public_keys.add(pkey)
						i=0
						for singlekey in keys.get(request.user.username).get(settings.KEY_VERSION).get('transmissionsKeys'):
							tkey=Transmissionkey.objects.create(version=settings.KEY_VERSION,key=singlekey,number=i)
							user.transmission_keys.add(tkey)
							i += 1
						if i == settings.TRANSMISSION_NUM_KEYS:
							if request.session.get('friend', False) is not None:
								try:
									form_friend=request.session.get('friend', False)
									friend = UserProfile.objects.get(user__username=form_friend)
									friend.total_of_registered_friends += 1
									Friendship.objects.create(user=user,friend=friend)
									if (friend.total_of_registered_friends % settings.NUM_USERS_GIFT)==0:
										if friend.date_end_of_service is not None:
											friend.date_end_of_service += datetime.timedelta(30)
									if friend.total_of_registered_friends==settings.NUM_USERS_LIFETIME_GIFT:
										friend.date_end_of_service=None
									friend.save()
									del request.session['friend']
								except UserProfile.DoesNotExist:
									pass
							result='ok'
	else:
		return HttpResponseForbidden(_('YOU_HAVE_TO_LOGIN'))
	return HttpResponse(result)

@login_required()
def changepassword(request):
	if request.method == 'POST':
		form = ChangepasswordForm(request.POST)
		if form.is_valid():
			try:
				user = UserProfile.objects.get(user=request.user).user
				if user.check_password(form.cleaned_data['oldpassword']):
					user.set_password(form.cleaned_data['password'])
					user.save()
					logout(request)
					return HttpResponseRedirect('/login/')
				else:
					return HttpResponseForbidden(_('OLD_PASSWORD_IS_NOT_CORRECT'))
			except UserProfile.DoesNotExist:
				pass
	else:
		form = ChangepasswordForm()
	return render_to_response('registration/changepassword.html',{
		'form': form
	},context_instance = RequestContext(request))
	
def confirm_email(request,user,code):
	try:
		u = UserProfile.objects.get(user__username=user)
		if u.confirmedemail==False:
			email = u.user.email
			code_calculated=hashlib.sha224(settings.EMAIL_CODE+':'+user+':'+email).hexdigest()
			text=code_calculated
			if code==code_calculated:
				u.confirmedemail=True
				u.save()
				text = _('EMAIL_CONFIRMED')
			else:
				text = _('EMAIL_NOT_CONFIRMED_ERROR')
		else:
			text = _('EMAIL_ALREADY_CONFIRMED')
	except UserProfile.DoesNotExist:
		return HttpResponse(_('USER_NOT_FOUND'))
	return HttpResponse(text)