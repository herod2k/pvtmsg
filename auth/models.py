from django import forms
from passwords.fields import PasswordField
from django.contrib.auth.models import User
from userprofile.models import UserProfile
from django.conf import settings
from django.utils.translation import ugettext_lazy
from captcha.fields import CaptchaField
from django.utils.translation import ugettext as _
import hashlib
from datetime import date

class RegistrationForm(forms.Form):
	username = forms.RegexField(regex=r'^[\w-]+$',label=ugettext_lazy("USERNAME"),max_length=20,required=True,help_text=ugettext_lazy('HELP_TEXT_USERNAME'),error_message = ugettext_lazy('USERNAME_CONTAIN_ONLY_LETTERS'),widget=forms.TextInput(attrs={'class': 'required'}))
	password = PasswordField(label=ugettext_lazy("PASSWORD"),help_text=ugettext_lazy('HELP_TEXT_PASSWORD'),widget=forms.PasswordInput(attrs={'class': 'required'}))
	password2 = PasswordField(label=ugettext_lazy("CONFIRM_PASSWORD"),help_text=ugettext_lazy('HELP_TEXT_CONFIRMPASSWORD'),widget=forms.PasswordInput(attrs={'class': 'required'}))
	language=forms.CharField(label=ugettext_lazy("SELECT_LANGUAGE"),widget=forms.Select(choices=settings.LANGUAGES,attrs={'class': 'required'}),help_text=ugettext_lazy('HELP_TEXT_LANGUAGES'))
	searchable = forms.BooleanField(label=ugettext_lazy("SEARCHABLE"),required=False,help_text=ugettext_lazy('HELP_TEXT_SEARCHABLES'),widget=forms.CheckboxInput(attrs={'class': 'notrequired'}))
	avatar = forms.ImageField(label=ugettext_lazy("AVATAR"),required=False,help_text=ugettext_lazy('HELP_TEXT_AVATAR'),widget=forms.FileInput(attrs={'class': 'notrequired'}))
	name = forms.CharField(label=ugettext_lazy("NAME"),max_length=40,required=False,help_text=ugettext_lazy('HELP_TEXT_NAME'),widget=forms.TextInput(attrs={'class': 'notrequired'}))
	surname = forms.CharField(label=ugettext_lazy("SURNAME"),max_length=40,required=False,help_text=ugettext_lazy('HELP_TEXT_SURNAME'),widget=forms.TextInput(attrs={'class': 'notrequired'}))
	email = forms.EmailField(label=ugettext_lazy("EMAIL"),required=False,help_text=ugettext_lazy('HELP_TEXT_EMAIL'),widget=forms.TextInput(attrs={'class': 'notrequired'}))
	notificationbyemail = forms.BooleanField(label=ugettext_lazy("NOTIFICATIONBYEMAIL"),required=False,help_text=ugettext_lazy('HELP_TEXT_NOTIFICATIONBYEMAIL'),widget=forms.CheckboxInput(attrs={'class': 'notrequired'}))
	showemail = forms.BooleanField(label=ugettext_lazy("SHOWEMAIL"),required=False,help_text=ugettext_lazy('HELP_TEXT_SHOWEMAIL'),widget=forms.CheckboxInput(attrs={'class': 'notrequired'}))
	friend = forms.CharField(label=ugettext_lazy("FRIEND"),max_length=20,required=False,help_text=ugettext_lazy('HELP_TEXT_FRIEND'),widget=forms.TextInput(attrs={'class': 'notrequired'}))
	policy = forms.BooleanField(label=ugettext_lazy("POLICY"),required=False,help_text=ugettext_lazy('HELP_TEXT_POLICY'),widget=forms.CheckboxInput())
	captcha = CaptchaField(label=ugettext_lazy("CAPTCHA"),required=True,help_text=ugettext_lazy('HELP_TEXT_CAPTCHA'))
	
	def clean_password(self):
		if self.data['password'] != self.data['password2']:
			raise forms.ValidationError(_('PASSWORD_ARE_NOT_THE_SAME'))
		return self.data['password']
	
	def clean_email(self):
		if len(self.data['email'])>0:
			if UserProfile.objects.filter(user__email=self.data['email']):
				raise forms.ValidationError(_('EMAIL_ALREADY_EXISTS'))
		return self.data['email']
	
	def clean_username(self):
		if User.objects.filter(username=self.data['username']):
			raise forms.ValidationError(_('USERNAME_ALREADY_EXISTS'))
		return self.data['username'].lower()
	
	def clean_friend(self):
		if len(self.data['friend'])>0:
			if User.objects.filter(username=self.data['friend']).count()==0:
				raise forms.ValidationError(_('FRIEND_NOT_EXISTS'))
		return self.data['friend']
	
	def clean_notificationbyemail(self):
		notificationbyemail = self.data.get('notificationbyemail',0)
		if notificationbyemail:
			if len(self.data['email'])==0:
				raise forms.ValidationError(_('NOTIFICATION_ONLY_WITH_EMAIL'))
		return notificationbyemail
		
	def clean_showemail(self):
		showemail = self.data.get('showemail',0)
		if showemail:
			if len(self.data['email'])==0:
				raise forms.ValidationError(_('SHOWEMAIL_ONLY_WITH_EMAIL'))
		return showemail
	
	def clean_policy(self):
		policy=self.data.get('policy',0)
		if policy==0:
				raise forms.ValidationError(_('YOU_HAVE_TO_ACCEPT_THE_POLICY'))
		return policy
	
	def clean(self,*args, **kwargs):
		self.clean_password()
		self.clean_username()
		self.clean_notificationbyemail()
		return super(RegistrationForm, self).clean(*args, **kwargs)
		
class ChangepasswordForm(forms.Form):
	oldpassword = PasswordField(label=ugettext_lazy("OLDPASSWORD"),help_text=ugettext_lazy('HELP_TEXT_OLDPASSWORD'),widget=forms.PasswordInput(attrs={'class': 'required'}))
	password = PasswordField(label=ugettext_lazy("PASSWORD"),help_text=ugettext_lazy('HELP_TEXT_PASSWORD'),widget=forms.PasswordInput(attrs={'class': 'required'}))
	password2 = PasswordField(label=ugettext_lazy("CONFIRM_PASSWORD"),help_text=ugettext_lazy('HELP_TEXT_CONFIRMPASSWORD'),widget=forms.PasswordInput(attrs={'class': 'required'}))
		
	def clean_password(self):
		if self.data['password'] != self.data['password2']:
			raise forms.ValidationError(_('PASSWORD_ARE_NOT_THE_SAME'))
		return self.data['password']

class send_activation_email():
	def get_link(self,data):
		code=hashlib.sha224(settings.EMAIL_CODE+':'+data['username']+':'+data['email']).hexdigest()
		link='http://%s/confirm/%s/%s'%(data['domain'],data['username'],code)
		return link
	
	def reset_database(self,data):
		tmp = UserProfile.objects.get(user__email=data['email'])
		tmp.email_confirmation_date=date.today()
		tmp.confirmedemail=False
		return tmp.save()
	
	def send(self,data):
		self.reset_database(data)
		link=self.get_link(data)
		email=data['email']
		subject=_('EMAIL_ADDRESS_CONFIRMATION_SUBJECT')
		text=_('EMAIL_ADDRESS_CONFIRMATION_TEXT %(link)s')%{'link':link}
		#attivare send_mail
		#return send_mail(subject,text, settings.EMAIL_REGISTRATION_ADDRESS,  [email], fail_silently=False)