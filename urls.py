from django.conf.urls.defaults import *
from django.conf import settings

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
	# Example:
	# (r'^pvtmsg/', include('pvtmsg.foo.urls')),
	(r'^i18n/', include('django.conf.urls.i18n')),

	# Uncomment the admin/doc line below to enable admin documentation:
	# (r'^admin/doc/', include('django.contrib.admindocs.urls')),

	# Uncomment the next line to enable the admin:
	(r'^admin/', include(admin.site.urls)),
	(r'^captcha/', include('captcha.urls')),
	(r'^login/', 'django.contrib.auth.views.login'),
	(r'^registration/', 'auth.views.registration1'),
	(r'^registration2/', 'auth.views.registration2'),
	(r'^changepassword/', 'auth.views.changepassword'),
	(r'^registration_keys/', 'auth.views.registration_keys'),
	(r'^get_public_key/', 'userprofile.views.get_publickey'),
	(r'^confirm/(?P<user>\w+)/(?P<code>\w+)$', 'auth.views.confirm_email'),
	(r'^messages/(?P<directory>\w+)/', 'messages.views.messages'),
	(r'^message/', 'messages.views.message'),
	(r'^readmessage/', 'messages.views.readmessage'),
	(r'^newmessage/', 'messages.views.newmessage'),
	(r'^sendmessage/', 'messages.views.sendmessage'),
	(r'^savedraft/', 'messages.views.savedraft'),
	(r'^deletemessage/', 'messages.views.deletemessage'),
	(r'^emptytrash/', 'messages.views.emptytrash'),
	(r'^friends/', 'userprofile.views.friends'), #temporaneao
	(r'^friendship/', 'userprofile.views.friend'), #temporaneao
	(r'^search_friend/', 'userprofile.views.searchfriend'), #temporaneao
	(r'^myprofile/', 'userprofile.views.profile'), #temporaneao
	(r'^add_friend/', 'userprofile.views.addfriend'), #temporaneao
	(r'^directories/', 'messages.views.directories'), #temporaneao
	(r'^directory/', 'messages.views.directory'), #temporaneao
)

import os
if settings.DEBUG:
    urlpatterns += patterns('',
        (r'^static/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT,'show_indexes': False}),
    )