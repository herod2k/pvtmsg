# -*- coding: utf-8 -*-
# Django settings for pvtmsg project.
import os

DEBUG = True
TEMPLATE_DEBUG = DEBUG

ADMINS = (
    # ('Your Name', 'your_email@domain.com'),
)

MANAGERS = ADMINS

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3', # Add 'postgresql_psycopg2', 'postgresql', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': 'test.db',                      # Or path to database file if using sqlite3.
        'USER': '',                      # Not used with sqlite3.
        'PASSWORD': '',                  # Not used with sqlite3.
        'HOST': '',                      # Set to empty string for localhost. Not used with sqlite3.
        'PORT': '',                      # Set to empty string for default. Not used with sqlite3.
    }
}

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# On Unix systems, a value of None will cause Django to use the same
# timezone as the operating system.
# If running in a Windows environment this must be set to the same as your
# system time zone.
TIME_ZONE = 'Europe/Madrid'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'it'

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True
gettext = lambda s: s
LANGUAGES = (
		('en', 'English'),
		('es', 'Español'),
		('it', 'Italiano'),
		('fr', 'Français'),
		('de', 'Deutsch'),
		('nl', 'Nederlands'),
	)

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale
USE_L10N = True

# Absolute path to the directory that holds media.
# Example: "/home/media/media.lawrence.com/"
MEDIA_ROOT = '%s/static/'%os.path.realpath(os.path.dirname(__file__)).replace('\\','/')

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash if there is a path component (optional in other cases).
# Examples: "http://media.lawrence.com", "http://example.com/media/"
MEDIA_URL = '/static/'

# URL prefix for admin media -- CSS, JavaScript and images. Make sure to use a
# trailing slash.
# Examples: "http://foo.com/media/", "/media/".
ADMIN_MEDIA_PREFIX = '/media/'

# Make this unique, and don't share it with anybody.
SECRET_KEY = '2uml^x)6s+)3jv&w(wu#bktr$wj--!=caci4m-fv!l4dpbcwv8'

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
#     'django.template.loaders.eggs.Loader',
)

MIDDLEWARE_CLASSES = (
	'django.contrib.sessions.middleware.SessionMiddleware',
	'django.middleware.locale.LocaleMiddleware',
	'django.middleware.common.CommonMiddleware',
	'django.middleware.csrf.CsrfViewMiddleware',
	'django.contrib.auth.middleware.AuthenticationMiddleware',
	'django.contrib.messages.middleware.MessageMiddleware',
)

TEMPLATE_CONTEXT_PROCESSORS = (
	'django.core.context_processors.i18n',
	'django.contrib.auth.context_processors.auth',
	'django.core.context_processors.media',
)

ROOT_URLCONF = 'pvtmsg.urls'


TEMPLATE_DIRS = (
    # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    '%s/templates'%os.path.realpath(os.path.dirname(__file__)).replace('\\','/')
)

INSTALLED_APPS = (
	'django.contrib.auth',
	'django.contrib.contenttypes',
	'django.contrib.sessions',
	'django.contrib.sites',
	'django.contrib.messages',
	# Uncomment the next line to enable the admin:
	'django.contrib.admin',
	# Uncomment the next line to enable admin documentation:
	# 'django.contrib.admindocs',
	'auth',
	'messages',
	'userprofile',
	'captcha',
)

#User profile class
AUTH_PROFILE_MODULE = 'userprofile.UserProfile'

# Login and registration configuration
LOGIN_REDIRECT_URL = '/messages'
LOGIN_URL = '/login'
PASSWORD_MIN_LENGTH = 8
PASSWORD_MATCH_THRESHOLD = 1.0
PASSWORD_COMPLEXITY = { # You can ommit any or all of these for no limit for that particular set
	"UPPER": 1,       # Uppercase
	"LOWER": 1,       # Lowercase
	"DIGITS": 1,      # Digits
	"PUNCTUATION": 1, # Punctuation (string.punctuation)
	"NON ASCII": 0,   # Non Ascii (ord() >= 128)
	"WORDS": 0        # Words (substrings seperates by a whitespace)
}
EMAIL_CODE='h2kregistration' #code to create link to activate email
EMAIL_REGISTRATION_ADDRESS='noreplay@djherod.com' #activate email address

# Captcha configuration
CAPTCHA_CHALLENGE_FUNCT = 'captcha.helpers.math_challenge'

# Key domain
KEY_DOMAIN='privatemessage.to'

# Key version
KEY_VERSION='0.1'

# Transmission key configuration
TRANSMISSION_NUM_KEYS = 60

# Avatar folder
MEDIA_AVATARS = '%savatars/'%(MEDIA_ROOT)

# Attachments folder
MEDIA_ATTACHMENTS = '%s/attachments/'%os.path.realpath(os.path.dirname(__file__)).replace('\\','/')

# key folder
SERVER_KEYS = '%s/serverkeys/'%os.path.realpath(os.path.dirname(__file__)).replace('\\','/')

# key
SERVER_PRIVATE_KEY = "%skey%s.pem"%(SERVER_KEYS,KEY_VERSION)
PASSWORD_RSA_KEY = '?3*\<<]#I2{1sk.'

#number of users for one month gift
NUM_USERS_GIFT = 5

#number of users for timelife account gift
NUM_USERS_LIFETIME_GIFT = 100

#browser version accepted
BROWSERS = {'opera':11.52,'firefox':8.0,'chrome':15}