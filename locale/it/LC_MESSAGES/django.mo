��          �      �           	          !     '     8     R     b     v     �     �     �     �     �     �     �     
       
   -     8     H     P     Y  B  q     �     �     �  /   �       "   ,  (   O     x  P   �  �   �  >   �     �      �     �               !     =     I     i     �  <   �                                           	          
                                                   AVATAR CONFIRM_PASSWORD EMAIL HELP_TEXT_AVATAR HELP_TEXT_CONFIRMPASSWORD HELP_TEXT_EMAIL HELP_TEXT_LANGUAGES HELP_TEXT_NAME HELP_TEXT_NOTIFICATIONBYEMAIL HELP_TEXT_PASSWORD HELP_TEXT_SEARCHABLES HELP_TEXT_SURNAME HELP_TEXT_USERNAME NAME NOTIFICATIONBYEMAIL PASSWORD PASSWORD_ARE_NOT_THE_SAME SEARCHABLE SELECT_LANGUAGE SURNAME USERNAME USERNAME_ALREADY_EXISTS Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2011-06-23 20:19+0200
PO-Revision-Date: 2011-06-23 20:26+0100
Last-Translator: nobody <nobody@djherod.com>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 Immagine del profilo Conferma la password E-mail Selezionare una immagine per il proprio profilo Reinserire la password Inserite un vostro contatto e-mail Selezionare la lingua del sito internet. Indicate il vostro nome Indicate se volete ricevere le notifiche dei messaggi al vostro indirizzo e-mail Scegliere la propria password, la password deve contenere almeno 8 caratteri, di cui almeno 1 maiuscolo, 1 minuscolo, 1 numero e un carattere di punteggiatura. Indicare se si vuole essere risultare da un'eventuale ricerca. Inserite il vostro cognome Scegliere il proprio nome utente Nome Ricevere le notifiche? Password Le passwords non coincidono Ricercabile Selezionare la lingua preferita Indicate il vostro cognome Nome Utente Il nome utente scelto è stato già assegnato in precedenza. 