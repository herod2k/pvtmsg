importScripts('../../config.js');
importScripts('../ajax.js');
importScripts('../library.js');
importScripts('../cryptico.js');
self.addEventListener('message', function(e) {
	var data = e.data;
	switch (data.action) {
	case 'sendMessage':
		var result={'success':false,'data':{'error':0}};
		if (checklogin(data.csrfmiddlewaretoken)) {
			var friendRsaPublicKey=getPublicKey(data.csrfmiddlewaretoken,data.message.recipient);
			if (friendRsaPublicKey.result=='ok'){
				var privateKey=getPrivateKey(data.password,data.privateKey,friendRsaPublicKey.myusername);
				if (privateKey.success){
					privateKey=privateKey.data.privateKey;
					var rsakey=createRSAKeyFromHex(privateKey[friendRsaPublicKey.myusername][key_version]['RSAKey']);
					var EncryptionResult = cryptico.encrypt(data.message.message, friendRsaPublicKey.public_key,rsakey);
					if (EncryptionResult.status=='success') {
						var dataToSent= {'message':EncryptionResult.cipher,
													'subject':data.message.subject,
													'from':friendRsaPublicKey.myusername,
													'to':data.message.recipient,
													'important':data.message.important,
													'oldid':data.message.oldid,
													'save_as_sent':data.message.save_as_sent,
													'relation':data.message.relation};
						if (data.message.save_as_sent) {
							var sentkey_num=Math.floor(Math.random() * num_sent_keys);
							var sentkey_key=privateKey[friendRsaPublicKey.myusername][key_version]['sentKeys'][sentkey_num];
							dataToSent['saved_sent']=cryptico.encryptAESCBC(data.message.message,HexStringToArray(sha256.hex(sentkey_key)));
							dataToSent['saved_sent_key']=sentkey_num;
						}
						dataToSent=JSON.stringify(dataToSent)
						var transmission_key_num=Math.floor(Math.random() * num_transmission_keys);
						var transmission_key=privateKey[friendRsaPublicKey.myusername][key_version]['transmissionsKeys'][transmission_key_num];
						var MessageToSend=cryptico.encryptAESCBC(dataToSent,HexStringToArray(sha256.hex(transmission_key)));
						var shipment = sendMessageTo(data.csrfmiddlewaretoken,MessageToSend,transmission_key_num);
						if (shipment.result=='ok'){
							result={'success':true,'data':{}};
						} else result={'success':false,'data':{'error':11}};
						delete privateKey;
						delete EncryptionResult;
						delete rsakey;
						delete data;
					} else result={'success':false,'data':{'error':6}};
				} else result={'success':false,'data':{'error':privateKey.data.error}};
			} else result={'success':false,'data':{'error':2}};
		} else result={'success':false,'data':{'error':1}};
		self.postMessage(result);
		break;
		
		case 'saveDraft':
			var result={'success':false,'data':{'error':0}};
			if (checklogin(data.csrfmiddlewaretoken)) {
				var friendRsaPublicKey=getPublicKey(data.csrfmiddlewaretoken,data.message.recipient);
				if (friendRsaPublicKey.result=='ok'){
					var privateKey=getPrivateKey(data.password,data.privateKey,friendRsaPublicKey.myusername);
					if (privateKey.success){
						privateKey=privateKey.data.privateKey;
							var sentkey_num=Math.floor(Math.random() * num_sent_keys);
							var sentkey_key=privateKey[friendRsaPublicKey.myusername][key_version]['sentKeys'][sentkey_num];
							var message=cryptico.encryptAESCBC(data.message.message,HexStringToArray(sha256.hex(sentkey_key)));
							var dataToSent= {'message':message,
														'subject':data.message.subject,
														'from':friendRsaPublicKey.myusername,
														'to':data.message.recipient,
														'important':data.message.important,
														'oldid':data.message.oldid,
														'relation':data.message.relation,
														'saved_sent_key':sentkey_num};
							
							dataToSent=JSON.stringify(dataToSent)
							var transmission_key_num=Math.floor(Math.random() * num_transmission_keys);
							var transmission_key=privateKey[friendRsaPublicKey.myusername][key_version]['transmissionsKeys'][transmission_key_num];
							var MessageToSend=cryptico.encryptAESCBC(dataToSent,HexStringToArray(sha256.hex(transmission_key)));
							var shipment = saveDraftTo(data.csrfmiddlewaretoken,MessageToSend,transmission_key_num);
							if (shipment.result=='ok'){
								result={'success':true,'data':{'message_uuid':shipment.message_uuid}};
							} 	else result={'success':false,'data':{'error':12}};
							delete privateKey;
							delete EncryptionResult;
							delete rsakey;
							delete data;
					} else result={'success':false,'data':{'error':privateKey.data.error}};
				} else result={'success':false,'data':{'error':2}};
			} else result={'success':false,'data':{'error':1}};
			self.postMessage(result);
			break;
			
		case 'readMessage':
			var result={'success':false,'data':{'error':0}};
			if (checklogin(data.csrfmiddlewaretoken)) {
				var friendRsaPublicKey=getPublicKey(data.csrfmiddlewaretoken,'server');
				if (friendRsaPublicKey.result=='ok'){
					var privateKey=getPrivateKey(data.password,data.privateKey,friendRsaPublicKey.myusername);
					if (privateKey.success){
						privateKey=privateKey.data.privateKey;
						var message=readMessageFrom(data.csrfmiddlewaretoken,data.idmessage);
						if (message.result=='ok'){
							var transmissionsKey=privateKey[friendRsaPublicKey.myusername][key_version]['transmissionsKeys'][message.data.transmission_key_num];
							var uncrypted = cryptico.decryptAESCBC(urlsafe_b64decode(message.data.message),HexStringToArray(sha256.hex(transmissionsKey)));
							uncrypted=JSON.parse(uncrypted.replace(/}[@]+/g, "}"));
							var signature='without';
							if (uncrypted.saveKey==null) {
								var rsakey=createRSAKeyFromHex(privateKey[friendRsaPublicKey.myusername][key_version]['RSAKey']);
								var personal_message=cryptico.decrypt(uncrypted.text, rsakey);
								uncrypted.text='';
								if (personal_message.status=="success") {
									uncrypted.text=personal_message.plaintext;
									var friendRsaPublicKey=getPublicKey(data.csrfmiddlewaretoken,uncrypted.from);
									if (friendRsaPublicKey.result=='ok'){
										if (cryptico.publicKeyID(personal_message.publicKeyString)==cryptico.publicKeyID(friendRsaPublicKey.public_key)) {
											signature=personal_message.signature;
										} else {
											signature='altered';
										}
									} else result={'success':false,'data':{'error':2}};
								} else result={'success':false,'data':{'error':6}};
							} else {
								var sentKey=privateKey[friendRsaPublicKey.myusername][key_version]['sentKeys'][uncrypted.saveKey];
								uncrypted.text=cryptico.decryptAESCBC(uncrypted.text,HexStringToArray(sha256.hex(sentKey)));
							}
							result={'success':true,'data':{'message':uncrypted,'signature':signature}};
						} else result={'success':false,'data':{'error':13}};
					} else result={'success':false,'data':{'error':privateKey.data.error}};
				} else result={'success':false,'data':{'error':2}};
			} else result={'success':false,'data':{'error':1}};
			self.postMessage(result);
			break;
	default:
		self.postMessage({'success':false,'data':{'error':0}});
	};
}, false);