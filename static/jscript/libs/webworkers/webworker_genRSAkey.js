importScripts('../../config.js');
importScripts('../ajax.js');
importScripts('../library.js');
importScripts('../cryptico.js');
self.addEventListener('message', function(e) {
	var data = e.data;
	switch (data.action) {
	case 'generateKey':
		var result={'success':false,'data':{}};
		if (e.data.csrfmiddlewaretoken!=null) {
			var keyserver=getPublicKey(e.data.csrfmiddlewaretoken,'server');
			if (keyserver.result=='ok'){
				var RSApwd = generate_password(RSA_key_lenght);
				var RSAkey = cryptico.generateRSAKey(RSApwd, RSA_lenght);
				var PublicKeyString = cryptico.publicKeyString(RSAkey);
				var transmissionsKeys = generate_passwords(num_transmission_keys,transmission_keys_lenght);
				var filesKeys = generate_passwords(num_files_keys,files_keys_lenght);
				var calendarKeys = generate_passwords(num_calendar_keys,calendar_keys_lenght);
				var addressbookKeys = generate_passwords(num_addressbook_keys,addressbook_keys_lenght);
				var sentKeys = generate_passwords(num_sent_keys,sent_keys_lenght);
				
				var keys = new Object();
				keys[keyserver.myusername]={};
				keys[keyserver.myusername][key_version]={'domain':domain_name,'RSAKey':convertRSAKeyToHex(RSAkey),'RSApwd':RSApwd,'transmissionsKeys':transmissionsKeys,'filesKeys':filesKeys,'calendarKeys':calendarKeys,'addressbookKeys':addressbookKeys,'sentKeys':sentKeys}
				
				var publickeys = new Object();
				publickeys[keyserver.myusername] = {};
				publickeys[keyserver.myusername][key_version]={'domain':domain_name,'PublicKeyString':PublicKeyString,'transmissionsKeys':transmissionsKeys};
				
				var keys=JSON.stringify(keys);
				var publickeys=JSON.stringify(publickeys);
				
				delete RSApwd;
				delete RSAkey;
				delete PublicKeyString;
				delete transmissionsKeys;
				delete filesKeys;
				delete calendarKeys;
				delete addressbookKeys;
				
				var keys_pwd=generate_password(6,true);
				var personalKey=cryptico.encryptAESCBC(keys,HexStringToArray(sha256.hex(keys_pwd)));
				var EncryptionResult = cryptico.encrypt(publickeys, keyserver.public_key);
				var result={'EncryptionResult':EncryptionResult};
				if (EncryptionResult.status=='success') {
					var result={'success':true,'data':{'publicKey':EncryptionResult.cipher,'personalKey':personalKey,'personalPwd':keys_pwd}};
				} else result={'success':false,'data':{'error':3}};
			} else result={'success':false,'data':{'error':2}};
			delete EncryptionResult;
			delete personalKey;
			delete keys_pwd;
			delete keyserver;
		} else result={'success':false,'data':{'error':1}};
		self.postMessage(result);
		break;
	default:
		self.postMessage({'success':false,'data':{'error':0}});
	};
}, false);