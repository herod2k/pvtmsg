var pvtmsg = (function() {

	var obj = {};

	obj.getCookie= function ( check_name ) {
		var a_all_cookies = document.cookie.split( ';' );
		var a_temp_cookie = '';
		var cookie_name = '';
		var cookie_value = '';
		var b_cookie_found = false;
		var i = '';
		
		for ( i = 0; i < a_all_cookies.length; i++ ) {
			a_temp_cookie = a_all_cookies[i].split( '=' );
			cookie_name = a_temp_cookie[0].replace(/^\s+|\s+$/g, '');
			if ( cookie_name == check_name ) {
				b_cookie_found = true;
				if ( a_temp_cookie.length > 1 ) {
					cookie_value = unescape( a_temp_cookie[1].replace(/^\s+|\s+$/g, '') );
				}
				return cookie_value;
				break;
			}
			a_temp_cookie = null;
			cookie_name = '';
		}
		if ( !b_cookie_found ) {
			return null;
		}
	}

	obj.randomFileName = function (length){
		var chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
		var pass = "";
		for(var x=0;x<length;x++) {
			var i = Math.floor(Math.random() * 62);
			pass += chars.charAt(i);
		}
		return pass;
	}

	obj.showPassword= function (container,pwd){
		var string='';
		var l=pwd.length;
		for (var i=0;i<l;i++){
			var c=pwd.substr(i,1);
			string += '<div id="id_block_'+(i+1)+'">'+c+'</div>';
		}
		$("#"+container).html(string);
	}

	obj.sendKey = function (publicKeys,okmsg,komsg){
		$.post('/registration_keys/',{ publicKeys:publicKeys, csrfmiddlewaretoken:obj.getCookie('csrftoken')}, function(data) {
			if (data=='ok'){
				alert(okmsg);
			} else {
				alert(komsg);
			}
			$("#downloadify").remove();
		});
	}

	obj.generateKey = function (directory,errors){
		var worker = new Worker(directory+'jscript/libs/webworkers/webworker_genRSAkey.js');
		worker.addEventListener('message', function(e) {
			if (e.data.success==true) {
				rsaKey=e.data;
				delete e;
				$("#key_gen").remove();
				$("#downloadify").show();
				obj.showPassword('password',rsaKey.data.personalPwd);
				delete rsaKey.data.personalPwd;
			} else {
				alert(errors[e.data.data.error])
			}
			}, false);
		worker.postMessage({'action':'generateKey','csrfmiddlewaretoken':obj.getCookie('csrftoken')});
	}
	
	obj.sendMessage = function (directory,privateKey,password,message,errors){
		var worker = new Worker(directory+'jscript/libs/webworkers/webworker_message.js');
		worker.addEventListener('message', function(e) {
			if (e.data.success==true) {
				delete e;
			} else {
				alert(errors[e.data.data.error])
			}
			}, false);
		worker.postMessage({'action':'sendMessage','csrfmiddlewaretoken':obj.getCookie('csrftoken'),'privateKey':privateKey,'password':password,'message':message});
	}
	
	obj.saveDraft = function (directory,privateKey,password,message,errors){
		var worker = new Worker(directory+'jscript/libs/webworkers/webworker_message.js');
		worker.addEventListener('message', function(e) {
			if (e.data.success==true) {
				$('#id_oldid').val(e.data.data.message_uuid);
				delete e;
			} else {
				alert(errors[e.data.data.error])
			}
			}, false);
		worker.postMessage({'action':'saveDraft','csrfmiddlewaretoken':obj.getCookie('csrftoken'),'privateKey':privateKey,'password':password,'message':message});
	}
	
	obj.readMessage = function (directory,privateKey,password,idmessage,errors){
		var worker = new Worker(directory+'jscript/libs/webworkers/webworker_message.js');
		worker.addEventListener('message', function(e) {
			if (e.data.success==true) {
				$('#id_oldid').val(e.data.data.message.uuid);
				$('#id_subject').val(e.data.data.message.subject);
				$('#id_message').val(e.data.data.message.text);
				console.log(e.data.data);
				delete e;
			} else {
				alert(errors[e.data.data.error])
			}
			}, false);
		worker.postMessage({'action':'readMessage','csrfmiddlewaretoken':obj.getCookie('csrftoken'),'privateKey':privateKey,'password':password,'idmessage':idmessage});
	}
	
	obj.getUrlParam =function(name){
		var regexS = "[\\?&]"+name+"=([^&#]*)";
		var regex = new RegExp ( regexS );
		var tmpURL = window.location.href;
		var results = regex.exec( tmpURL );
		if( results == null )
			return"";
		else
			return results[1];
	}
	
	return obj;
	
}());