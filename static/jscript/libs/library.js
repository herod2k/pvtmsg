function generate_password(ln, simple){
	simple = typeof(simple) != 'undefined' ? simple : false;
	if (simple) 
		string = simple_string_password
	else
		string = complex_string_password
	n=string.length
	pwd=""
	for (i=1;i<=ln;i++){
		pwd +=string.substr(Math.floor(Math.random() * n),1);
	}
	return pwd
}

function generate_passwords(n,ln){
	var keys = new Array();
	for ( var i=0; i<n; i++ ){
		keys.push(generate_password(ln));
	}
	return keys;
}

function HexStringToArray(HexString){
	var result = new Array();
	for (var i = 0; i < HexString.length/2; i++) {
		h=HexString.substr(i*2,2);
		n=parseInt(h,16);
		result.push(n);
	}
	return result;
}

function getPublicKey(csrfmiddlewaretoken,user){
	result=ajax.json('/get_public_key/?user='+user+'&csrfmiddlewaretoken='+csrfmiddlewaretoken);
	return result;
}

function convertRSAKeyToHex(rsakey){
	tmp = {}
	tmp.n = rsakey.n.toString(16);
	tmp.e = rsakey.e.toString(16);
	tmp.d = rsakey.d.toString(16);
	tmp.p = rsakey.p.toString(16);
	tmp.q = rsakey.q.toString(16);
	tmp.dmp1 = rsakey.dmp1.toString(16);
	tmp.dmq1 = rsakey.dmq1.toString(16);
	tmp.coeff = rsakey.coeff.toString(16);
	return tmp;
}

function createRSAKeyFromHex(rsakeyhex){
	var rsa = new RSAKey();
	rsa.setPrivateEx(rsakeyhex.n, rsakeyhex.e, rsakeyhex.d, rsakeyhex.p, rsakeyhex.q, rsakeyhex.dmp1, rsakeyhex.dmq1, rsakeyhex.coeff);
	return rsa;
}

function IsJsonString(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}

function sendMessageTo(csrfmiddlewaretoken,text,transmissionKey){
	result=ajax.json('/sendmessage/?csrfmiddlewaretoken='+csrfmiddlewaretoken+'&transmissionKey='+transmissionKey+'&text='+urlsafe_b64encode(text));
	return result;
}

function saveDraftTo(csrfmiddlewaretoken,text,transmissionKey){
	result=ajax.json('/savedraft/?csrfmiddlewaretoken='+csrfmiddlewaretoken+'&transmissionKey='+transmissionKey+'&text='+urlsafe_b64encode(text));
	return result;
}

function readMessageFrom(csrfmiddlewaretoken,idmessage){
	result=ajax.json('/message/?csrfmiddlewaretoken='+csrfmiddlewaretoken+'&idmessage='+idmessage);
	return result;
}

function urlsafe_b64encode(string){
	tmp='';
	for (i=0;i<=string.length-1;i++) {
		l=string.substr(i,1);
		switch(l) {
			case '+':
				tmp += '-';
			break;
			case '/':
				tmp += '_';
			break;
			default:
				tmp += l;
			break;
		}
		
	}
	return tmp
}

function urlsafe_b64decode(string){
	tmp='';
	for (i=0;i<=string.length-1;i++) {
		l=string.substr(i,1);
		switch(l) {
			case '-':
				tmp += '+';
			break;
			case '_':
				tmp += '/';
			break;
			default:
				tmp += l;
			break;
		}
		
	}
	return tmp
}

function checklogin(csrfmiddlewaretoken) {
	if (csrfmiddlewaretoken!='')
		return true;
	else
		return false;
}

function getPrivateKey(password,privateKeyEnc,myusername){
	result={'success':false,'data':{'error':0}}
	if (privateKeyEnc!='undefined') {
		if (password!='') {
			var aeskey = HexStringToArray(sha256.hex(password));
			var privateKey = cryptico.decryptAESCBC(privateKeyEnc, aeskey);
			if (IsJsonString(privateKey)) {
				privateKey=JSON.parse(privateKey);
				if (privateKey[myusername][key_version] != 'undefined') {
					if (privateKey[myusername][key_version]['domain']==domain_name) {
						result={'success':true,'data':{'privateKey':privateKey}}
					} else result={'success':false,'data':{'error':5}};
				} else result={'success':false,'data':{'error':4}};
			} else result={'success':false,'data':{'error':3}};
		}else result={'success':false,'data':{'error':9}};
	}else result={'success':false,'data':{'error':10}};
	return result;
}

