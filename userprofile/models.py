from django.db import models
from django.contrib.auth.models import User
from django.conf import settings
from django.template.defaultfilters import slugify
from messages.models import Message,Directory,Attachment
from uuidfield import UUIDField
from os import path
import time

# Create your models here.
class Publickey(models.Model):
	version = models.CharField(max_length=5)
	key_string = models.TextField()
	insert_date = models.DateTimeField(auto_now_add=True)
	def __unicode__(self):
		return "%s %s"%(self.version,self.key_string[0:10])

class Transmissionkey(models.Model):
	version = models.CharField(max_length=5)
	key = models.CharField(max_length=50)
	number = models.IntegerField(max_length=2)
	insert_date = models.DateTimeField(auto_now_add=True)
	def __unicode__(self):
		return "%s %s %s"%(self.version,self.key[0:10],self.number)

class Payment(models.Model):
	TYPE_CHOICES = (
		('paypal', 'Paypal'),
		('googlco', 'Google Check Out'),
		('ccard', 'Credit Card'),
	)
	active = models.BooleanField()
	user = models.ForeignKey(User,verbose_name="user")
	type = models.CharField(max_length=15, choices=TYPE_CHOICES, verbose_name="payment type")
	lifetime = models.BooleanField(verbose_name="lifetime account")
	paymentDate = models.DateField(verbose_name="payment date")
	paymentEnd = models.DateField(verbose_name="payment end")
	reference = models.CharField(max_length=50, verbose_name="payment reference", null=True,blank=True)
	notes = models.TextField(null=True,blank=True, verbose_name="payment additional notes")
	def __unicode__(self):
		return self.user

class UserProfile(models.Model):
	def get_upload_to(instance, filename):
		base, ext = path.splitext(path.basename(filename))
		return '%s%f%s' % (settings.MEDIA_AVATARS,time.time(),ext.lower())
	uuid = UUIDField(auto=True)
	searchable = models.BooleanField()
	user = models.OneToOneField(User)
	avatar = models.ImageField(upload_to=get_upload_to,null=True,blank=True)
	showemail = models.BooleanField(verbose_name="show email to others users")
	notificationbyemail = models.BooleanField(verbose_name="notifications by email")
	confirmedemail = models.BooleanField(verbose_name="email address confirmed", default=False)
	email_confirmation_date = models.DateField(verbose_name="email for confirmation sended",null=True,blank=True)
	language = models.CharField(max_length=2, choices=settings.LANGUAGES,verbose_name="user language")
	showfriendships = models.BooleanField(verbose_name="show friendships")
	friends= models.ManyToManyField('self',related_name="user_friends",null=True,blank=True)
	friends_registered = models.ManyToManyField('self',related_name="user_friends_registered",null=True,blank=True,symmetrical=False)
	total_of_registered_friends = models.IntegerField(default=0)
	public_keys=models.ManyToManyField(Publickey,null=True,blank=True)
	transmission_keys=models.ManyToManyField(Transmissionkey,null=True,blank=True)
	directories=models.ManyToManyField(Directory,null=True,blank=True)
	messages=models.ManyToManyField(Message,null=True,blank=True)
	attachments = models.ManyToManyField(Attachment,null=True,blank=True)
	payments = models.ManyToManyField(Payment,null=True,blank=True)
	date_end_of_service = models.DateTimeField(null=True,blank=True)
	def __unicode__(self):
		return self.user.username
	class Meta:
		ordering = ['user__username']

class Friendship(models.Model):
	uniqueid = UUIDField(auto=True)
	accepted = models.BooleanField()
	refused = models.BooleanField()
	user = models.ForeignKey(UserProfile,related_name="profile",verbose_name="user")
	friend = models.ForeignKey(UserProfile,related_name="friend",verbose_name="friend")
	petitionDate = models.DateField(verbose_name="petition",auto_now_add=True)
	updated = models.DateField(verbose_name="updated",auto_now=True)
	def __unicode__(self):
		return "%s %s"%(self.user,self.friend)

