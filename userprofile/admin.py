from userprofile.models import Friendship,Payment,UserProfile,Publickey,Transmissionkey
from django.contrib import admin

admin.site.register(UserProfile)
admin.site.register(Friendship)
admin.site.register(Payment)
admin.site.register(Publickey)
admin.site.register(Transmissionkey)
