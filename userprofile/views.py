# Create your views here.
import sys,json
from django.shortcuts import render_to_response
from django.http import HttpResponseForbidden,HttpResponse
from django.contrib.auth.decorators import login_required
from django.template import RequestContext
from models import *
from django.contrib.auth.models import User
from django.db.models import Q
from django.utils.translation import ugettext as _
from django.conf import settings
from datetime import date

@login_required
def get_publickey(request):
	result=json.dumps({'result':'ko','public_key':''})
	if request.user.is_authenticated():
		user_pub_rsa=request.GET.get('user')
		if user_pub_rsa is not None:
			if user_pub_rsa=='server' or len(user_pub_rsa)>0:
				result='ok'
				try:
					if user_pub_rsa=='server':
						public_key=UserProfile.objects.get(user__username=user_pub_rsa).public_keys.get(version=settings.KEY_VERSION)
					else:
						public_key=UserProfile.objects.get(user=request.user).friends.get(user__username=user_pub_rsa).public_keys.get(version=settings.KEY_VERSION)
					result=json.dumps({'result':'ok','public_key':public_key.key_string,'myusername':request.user.username})
				except UserProfile.DoesNotExist:
					result=json.dumps({'result':'ko','public_key':'','myusername':request.user.username})
	else:
		return HttpResponseForbidden(_('YOU_HAVE_TO_LOGIN'))
	return HttpResponse(result)

@login_required()
def friends(request):
	if request.user.is_authenticated():
		try:
			friends=UserProfile.objects.get(user=request.user).friends.exclude(user__username='server').order_by('user__username')
		except UserProfile.DoesNotExist:
			friends=()
		friendships=Friendship.objects.filter(friend__user=request.user,accepted=False,refused=False).exclude(friend__user__username='server').order_by('friend__user__username')
	return render_to_response('friends/home.html',{'friends':friends,'friendships':friendships})

@login_required()
def searchfriend(request):
	if request.user.is_authenticated():
		if request.method == 'GET':
			search=request.GET.get('search')
			if (search is not None) and (len(search)>2):
				users=UserProfile.objects.filter(Q(searchable=True),Q(user__username__contains=search)|Q(user__first_name__contains=search)|Q(user__last_name__contains=search)|(Q(user__email__contains=search)&Q(showemail=True))).exclude(user__username='server').exclude(user=request.user).order_by('user__username')
				return render_to_response('friends/search.html',{'users':users}, context_instance = RequestContext(request))
			else:
				return HttpResponse('')
	return HttpResponseForbidden(_('YOU_ARE_NOT_AUTHORIZED'))

@login_required()
def addfriend(request):
	result='ko'
	friend=request.GET.get('friend')
	if len(friend)>2:
		if len(Friendship.objects.filter((Q(user__user=request.user)&Q(friend__user__username=friend))|(Q(user__user__username=friend)&Q(friend__user=request.user)))) == 0:
			try:
				user=UserProfile.objects.get(user=request.user)
				friend=UserProfile.objects.get(user__username=friend,searchable=True)
				Friendship.objects.create(user=user,friend=friend)
				result='ok'
			except UserProfile.DoesNotExist:
				result='ko'
		else:
			return HttpResponseForbidden(_('YOU_HAVE_ALREADY_ASK_IT'))
	return HttpResponse(result)

@login_required()
def friend(request):
	action=request.GET.get('action')
	uniqueid=request.GET.get('id')
	result='ko'
	try:
		f=Friendship.objects.get(Q(uniqueid=uniqueid),Q(user__user=request.user)|Q(friend__user=request.user))
		if action is not None:
			if action == 'accept':
				f.accepted=True
				f.save()
				try:
					UserProfile.objects.get(user=f.user).friends.add(f.friend)
					UserProfile.objects.get(user=f.friend).friends.add(f.user)
					result='ok'
				except UserProfile.DoesNotExist:
					pass
			if action == 'refuse':
				f.refused=True
				f.save()
				result='ok'
	except Friendship.DoesNotExist:
		return HttpResponseForbidden(_('YOU_ARE_NOT_AUTHORIZED'))
	return HttpResponse(result)
	
@login_required()
def profile(request):
	try:
		user=UserProfile.objects.get(user=request.user)
	except UserProfile.DoesNotExist:
		return HttpResponseForbidden(_('YOU_ARE_NOT_AUTHORIZED'))
	return render_to_response('profile/home.html',{'user':user}, context_instance = RequestContext(request))