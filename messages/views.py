# Create your views here.
import json,hashlib,random,string
from django.shortcuts import render_to_response
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_protect
from django.contrib.auth.decorators import login_required
from django.utils.translation import ugettext as _
from django.http import HttpResponseForbidden,HttpResponse
from django.template import RequestContext
from models import *
from forms import *
from userprofile.models import UserProfile,Transmissionkey
from django.conf import settings
from django.core import serializers

@login_required()
def newmessage(request):
	form = SendMessageForm(initial={'sent':True},request=request)
	return render_to_response('messages/new.html',{
		'form': form
	},context_instance = RequestContext(request))



@login_required()
def sendmessage(request):
	result=json.dumps({'result':'ko'})
	if request.method == 'GET':
		transmission_key_num=request.GET.get('transmissionKey');
		encapsulated_text=request.GET.get('text');
		key=UserProfile.objects.get(user=request.user)
		
		try:
			transmission_key=UserProfile.objects.get(user=request.user).transmission_keys.get(version=settings.KEY_VERSION,number=transmission_key_num).key;
			from Crypto.Cipher import AES
			m = hashlib.sha256()
			m.update(transmission_key)
			transmission_key_sha=m.digest()
			decryptor = AES.new(transmission_key_sha, AES.MODE_CBC)
			message = decryptor.decrypt(encapsulated_text.replace('-','+').replace('_','/').decode("base64"))
			message=message[message.find('{'):(message.rfind('}')+1)]
			message=json.loads(message)
			if (request.user.username==message.get('from','')):
				user=UserProfile.objects.get(user=request.user)
				if message.get('oldid','') !='':
					try:
						msd=user.messages.get(uuid__exact=message.get('oldid',''),draft=True)
						msd.delete()
					except Message.DoesNotExist:
						pass
				try:
					friend=UserProfile.objects.get(user__username__exact=message.get('to',''))
					m=Message.objects.create(fromUsr=user,toUsr=friend,subject=message.get('subject',''),text=message.get('message',''),important=message.get('important',0),key_version=settings.KEY_VERSION)
					friend.messages.add(m)
					if message.get('save_as_sent',False):
						ms=Message.objects.create(fromUsr=user,toUsr=friend,subject=message.get('subject',''),text=message.get('saved_sent',''),saveKey=message.get('saved_sent_key',0),important=message.get('important',0),key_version=settings.KEY_VERSION,uuid_outbox=m.uuid)
						user.messages.add(ms)
						m.sent_uuid=ms.uuid
						m.save()
					result=json.dumps({'result':'ok'})
				except UserProfile.DoesNotExist:
					pass
		except Transmissionkey.DoesNotExist:
			pass
	return HttpResponse(result)

@login_required()
def savedraft(request):
	result=json.dumps({'result':'ko'})
	if request.method == 'GET':
		transmission_key_num=request.GET.get('transmissionKey');
		encapsulated_text=request.GET.get('text');
		key=UserProfile.objects.get(user=request.user)
		
		try:
			transmission_key=UserProfile.objects.get(user=request.user).transmission_keys.get(version=settings.KEY_VERSION,number=transmission_key_num).key;
			from Crypto.Cipher import AES
			m = hashlib.sha256()
			m.update(transmission_key)
			transmission_key_sha=m.digest()
			decryptor = AES.new(transmission_key_sha, AES.MODE_CBC)
			message = decryptor.decrypt(encapsulated_text.replace('-','+').replace('_','/').decode("base64"))
			message=message[message.find('{'):(message.rfind('}')+1)]
			
			message=json.loads(message)
			result=message.get('message','')
			if (request.user.username==message.get('from','')):
				user=UserProfile.objects.get(user=request.user)
				friend=UserProfile.objects.get(user__username__exact=message.get('to',''))
				if message.get('oldid','') !='':
					try:
						msd=user.messages.get(uuid__exact=message.get('oldid',''),draft=True)
						msd.delete()
					except Message.DoesNotExist:
						pass
				try:
					ms=Message.objects.create(fromUsr=user,toUsr=friend,subject=message.get('subject',''),text=message.get('message',''),draft=True,saveKey=message.get('saved_sent_key',0),important=message.get('important',0),key_version=settings.KEY_VERSION)
					user.messages.add(ms)
					result=json.dumps({'result':'ok','message_uuid':ms.uuid})
				except UserProfile.DoesNotExist:
					pass
		except Transmissionkey.DoesNotExist:
			pass
	return HttpResponse(result)
	
@login_required()
def deletemessage(request):
	result='ko'
	if request.method == 'GET':
		idmessage=request.GET.get('idmessage','')
		definitely=request.GET.get('definitely',0)
		if idmessage is not None and len(idmessage)>5:
			try:
				m=UserProfile.objects.get(user=request.user).messages.get(uuid__exact=idmessage,deleted=False)
				m.deleted=True
				m.save()
				result='ok'
			except Message.DoesNotExist:
				pass
			if definitely=='1':
				try:
					m=UserProfile.objects.get(user=request.user).messages.get(uuid__exact=idmessage,deleted=True).delete()
					result='ok'
				except Message.DoesNotExist:
					pass
	return HttpResponse(result)

@login_required()
def emptytrash(request):
	result='ko'
	try:
		UserProfile.objects.get(user=request.user).messages.filter(deleted=True).delete()
		result='ok'
	except Message.DoesNotExist:
		pass
	return HttpResponse(result)

@login_required()
def message(request):
	result=json.dumps({'result':'ko'})
	transmission_key_num=random.randint(0,settings.TRANSMISSION_NUM_KEYS-1)
	transmission_key=UserProfile.objects.get(user=request.user).transmission_keys.get(version=settings.KEY_VERSION,number=transmission_key_num).key;
	from Crypto.Cipher import AES
	m = hashlib.sha256()
	m.update(transmission_key)
	transmission_key_sha=m.digest()
	encryptor = AES.new(transmission_key_sha, AES.MODE_CBC)
	idmessage=request.GET.get('idmessage','')
	try:
		message=UserProfile.objects.get(user=request.user).messages.get(uuid__exact=idmessage)
		messageToSend=json.dumps({'uuid':message.uuid,'from':message.fromUsr.user.username,'to':message.toUsr.user.username,'subject':message.subject,'text':message.text,'notes':message.notes,'saveKey':message.saveKey,'important':message.important,'relation':message.relation,'key_version':message.key_version,'created_date':str(message.createdDate),'read_date':str(message.readDate),'uuid_sent':message.uuid_sent,'uuid_outbox':message.uuid_outbox})
		messageToSend=("".join(random.sample(string.letters+string.digits, 16)))+messageToSend
		messageToSend=messageToSend+((16-len(messageToSend)%16)*'@')
		message = encryptor.encrypt(messageToSend).encode("base64").replace('+','-').replace('/','_')
		result=json.dumps({'result':'ok','data':{'transmission_key_num':transmission_key_num,'message':message}})
	except Message.DoesNotExist:
		pass
	return HttpResponse(result)

@login_required()
def readmessage(request):
	form = SendMessageForm(initial={'sent':True},request=request)
	return render_to_response('messages/message.html',{
		'form': form
	},context_instance = RequestContext(request))

@login_required()
def messages(request,directory):
	if directory=='inbox':
		messages=UserProfile.objects.get(user=request.user).messages.filter(toUsr__user=request.user,deleted=False,directory=None)
	elif directory=='sent':
		messages=UserProfile.objects.get(user=request.user).messages.filter(fromUsr__user=request.user,draft=False,deleted=False,directory=None)
	elif directory=='drafts':
		messages=UserProfile.objects.get(user=request.user).messages.filter(fromUsr__user=request.user,draft=True,deleted=False,directory=None)
	elif directory=='trash':
		messages=UserProfile.objects.get(user=request.user).messages.filter(deleted=True)
	else:
		messages=UserProfile.objects.get(user=request.user).messages.filter(draft=False,deleted=False,directory__slugfied=directory)
	return render_to_response('messages/home.html',{'messages':messages,'directory':directory,'user':request.user})

@login_required()
def directories(request):
	if request.user.is_authenticated():
		try:
			directories=UserProfile.objects.get(user=request.user).directories.all()
		except UserProfile.DoesNotExist:
			return HttpResponseForbidden(_('YOU_HAVE_TO_LOGIN'))
	return render_to_response('directories/home.html',{'directories':directories})

@login_required()
def directory(request):
	id=request.GET.get('id')
	name=request.GET.get('name')
	action=request.GET.get('action')
	result='ko'
	try:
		user=UserProfile.objects.get(user=request.user)
	except UserProfile.DoesNotExist:
		return HttpResponseForbidden(_('YOU_HAVE_TO_REGISTER'))
	if request.user.is_authenticated():
		if action is not None:
			if action in ('rename','add','delete'):
				if id is not None and name is not None:
					if len(id)>2 and len(name)>2 and action == 'rename':
						try:
							directory=user.directories.get(slugfied=id)
							directory.name=name
							directory.save()
							result='ok'
						except Directory.DoesNotExist:
							return HttpResponseForbidden(_('THE_FOLDER_DOESNT_EXIST'))
				if name is not None:
					if len(name)>2 and action == 'add':
						if user.directories.filter(name=name).count()==0:
							directory=Directory.objects.create(name=name)
							user.directories.add(directory)
							result='ok'
				if id is not None:
					if len(id)>0 and action == 'delete':
						try:
							directory=user.directories.get(slugfied=id)
							if request.GET.get('delete_files')=='yes':
								Message.objects.filter(directory=directory).update(deleted=True)
								Message.objects.filter(directory=directory).update(directory='')
							else:
								Message.objects.filter(directory=directory).update(directory='')
							directory.delete()
							result='ok'
						except Directory.DoesNotExist:
							return HttpResponseForbidden(_('THE_FOLDER_DOESNT_EXIST'))
	return HttpResponse(result)