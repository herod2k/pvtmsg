import uuid
from django.db import models
from userprofile.models import *
from django.conf import settings
from uuidfield import UUIDField

# Create your models here.
class Directory(models.Model):
	name = models.CharField(max_length=30,blank=False)
	slugfied = models.SlugField(null=True,blank=True)
	relation = models.ForeignKey('self',null=True,blank=True,verbose_name="child_of")
	def save(self, *args, **kwargs):
		self.slugfied = slugify(self.name)
		super(Directory, self).save(*args, **kwargs)
	def __unicode__(self):
		return self.name
	class Meta:
		ordering = ['name']

class Attachment(models.Model):
	uuid = UUIDField(auto=True)
	name = models.CharField(max_length=50)
	file = models.FileField(upload_to=settings.MEDIA_ATTACHMENTS,null=True,blank=True) #sistemare
	type = models.CharField(max_length=30)
	createdDate = models.DateTimeField(auto_now_add=True)
	def __unicode__(self):
		return self.name
	class Meta:
		ordering = ['name']

class Message(models.Model):
	uuid = UUIDField(auto=True)
	fromUsr = models.ForeignKey('userprofile.UserProfile',related_name="from_user",verbose_name="from User",null=True,blank=True)
	toUsr = models.ForeignKey('userprofile.UserProfile',related_name="to_user",verbose_name="to User",null=True,blank=True)
	subject = models.CharField(max_length=200,null=True,blank=True)
	text = models.TextField(null=True,blank=True)
	notes = models.TextField(null=True,blank=True)
	uuid_sent = models.TextField(null=True,blank=True)
	uuid_outbox = models.TextField(null=True,blank=True)
	lamportSign = models.TextField(null=True,blank=True)
	saveKey = models.IntegerField(max_length=2,null=True,blank=True)
	createdDate = models.DateTimeField(auto_now_add=True)
	readDate = models.DateTimeField(null=True,blank=True)
	directory = models.ForeignKey(Directory,null=True,blank=True)
	draft = models.BooleanField()
	deleted = models.BooleanField()
	important = models.BooleanField()
	relation = models.ForeignKey('self',null=True,blank=True,verbose_name="message related")
	attachments = models.ManyToManyField(Attachment,null=True,blank=True)
	key_version = models.CharField(max_length=5)
	def __unicode__(self):
		return "%s %s %s"%(self.fromUsr.user.username,self.toUsr.user.username,self.subject)
	class Meta:
		ordering = ['-createdDate']
	
