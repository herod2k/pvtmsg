from django import forms
from django.utils.translation import ugettext_lazy
from userprofile.models import UserProfile

class SendMessageForm(forms.Form):
	recipient = forms.ModelChoiceField(queryset='', empty_label=None,label=ugettext_lazy("RECIPIENT"),help_text=ugettext_lazy('HELP_TEXT_RECIPIENT'))
	subject = forms.CharField(label=ugettext_lazy("SUBJECT"),max_length=50,required=False,help_text=ugettext_lazy('HELP_TEXT_SUBJECT'),widget=forms.TextInput(attrs={'class': 'notrequired'}))
	message = forms.CharField(label=ugettext_lazy("MESSAGE"),required=True,help_text=ugettext_lazy('HELP_TEXT_MESSAGE'),widget=forms.Textarea(attrs={'class': 'required'}))
	important = forms.BooleanField(label=ugettext_lazy("IMPORTANT"),required=False,help_text=ugettext_lazy('HELP_TEXT_IMPORTANT'))
	sent = forms.BooleanField(label=ugettext_lazy("SAVE_SENT"),required=False,help_text=ugettext_lazy('HELP_TEXT_SAVE_SENT'))
	relation = forms.CharField(label=ugettext_lazy("RELATION"))
	oldid = forms.CharField(label=ugettext_lazy("OLDID"))
	def __init__(self, *args, **kwargs):
		self.request = kwargs.pop('request', None)
		super(SendMessageForm, self).__init__(*args, **kwargs)
		user = self.request.user
		self.fields['recipient'].queryset = UserProfile.objects.get(user=user).friends.all()