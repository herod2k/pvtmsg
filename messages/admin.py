from messages.models import Message,Directory,Attachment
from django.contrib import admin

admin.site.register(Message)
admin.site.register(Directory)
admin.site.register(Attachment)